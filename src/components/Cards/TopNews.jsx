import React from 'react';
import './TopNews.css';

const TopNews = (props) => {
    return (
        <div className='wrapper'>
            <div className='TopNews-Container'>
                <div className='TopNews-Thumbnail bg-red'>
                    <img 
                    src="https://e0.365dm.com/20/09/768x432/skysports-lionel-messi-barcelona_5113303.jpg?20200929233110" 
                    alt=""
                    height='100%'
                    />
                </div>
                <span className='TopNews-Desc'>{props.category}</span>
                <span className='TopNews-Title bg-yellow'>{props.title}</span>
                <span className='TopNews-Desc bg-yellow'>{props.desc}</span>
            </div>
        </div>
    );
}

TopNews.defaultProps = {
    category: 'category',
    title: 'Title',
    desc: '0 hours ago'


}

export default TopNews;
