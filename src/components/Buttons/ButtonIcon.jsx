import React, { useState } from "react";
import './ButtonPlain.css';

const icons = {
    'send': "M1.675 17.5L19.1667 10L1.675 2.5L1.66666 8.33333L14.1667 10L1.66666 11.6667L1.675 17.5Z",
    'share': "M15.5 13.4C14.8667 13.4 14.3 13.65 13.8667 14.0417L7.925 10.5833C7.96667 10.3917 8 10.2 8 10C8 9.80001 7.96667 9.60834 7.925 9.41667L13.8 5.99167C14.25 6.40834 14.8417 6.66667 15.5 6.66667C16.8833 6.66667 18 5.55001 18 4.16667C18 2.78334 16.8833 1.66667 15.5 1.66667C14.1167 1.66667 13 2.78334 13 4.16667C13 4.36667 13.0333 4.55834 13.075 4.75001L7.2 8.17501C6.75 7.75834 6.15833 7.50001 5.5 7.50001C4.11667 7.50001 3 8.61667 3 10C3 11.3833 4.11667 12.5 5.5 12.5C6.15833 12.5 6.75 12.2417 7.2 11.825L13.1333 15.2917C13.0917 15.4667 13.0667 15.65 13.0667 15.8333C13.0667 17.175 14.1583 18.2667 15.5 18.2667C16.8417 18.2667 17.9333 17.175 17.9333 15.8333C17.9333 14.4917 16.8417 13.4 15.5 13.4Z",


}


export default function ButtonIcon(props) {
    const [size] = useState(props.size);
    const [variant] = useState(props.variant);
    return (
        <div className='wrapper'>
                <svg width='22' height='22' viewBox="0 0 1024 1024">
                <path d={icons[props.icon]}></path>
                </svg>
                <button 
                className={`btn-${variant} btn-${size}`}>
                {props.children}
                </button>

        </div>
    )   
}

ButtonIcon.defaultProps = {
    children: 'ButtonPlain',
    size:'wide15b',
    variant:'red',
    icon:'share'
  }
