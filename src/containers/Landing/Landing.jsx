import React, { Component } from 'react'
import TopNews from '../../components/Cards/TopNews';
import ButtonPlain, {ButtonIcon} from '../../components/Buttons/Button.jsx';

class Landing extends Component {
    render() {
        return (
            <div>
                <TopNews 
                title='Messi Bala'
                category='Sports'
                desc='1 hours ago'
                />
                <TopNews />
                <TopNews />
                <ButtonPlain/>
                <ButtonIcon/>
            </div>
        )
    }
}


export default Landing;